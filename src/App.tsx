import React, { Component } from 'react';
import { Routes, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import logo from './logo.svg';
import './App.css';
import ObData from './components/Obdata';
import CIHome from './components/CIHome';
import ObList from './components/ObList';


function App() {
  return (
    <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <Link to={"/"} className="navbar-brand">
            CI Home
          </Link>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/obdata"} className="nav-link">
                ObData
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/oblist"} className="nav-link">
                ObList
              </Link>
            </li>
          </div>
        </nav>

        <div className="container mt-3">
          <Routes>
            <Route path="/" element={<CIHome />} />
          </Routes>
          <Routes>
            <Route path="/obdata" element={<ObData />} />
          </Routes>
          <Routes>
            <Route path="/oblist" element={<ObList />} />
          </Routes>
        </div>
      </div>
  );
}

export default App;
