import {Component} from "react"
import oblistDataService from "../services/oblist.service"
import Moment from 'moment';

type Props = {
    oblist: any,
    close: () => any
}

type State = {
    ins_date: any, 
    ID: any, 
    OBnumber: any, 
    OBname: string,
    Program: string, 
    Observer: string, 
    OBpriority: number, 
    OBactivation: any, 
    OBstartTime: any, 
    OBstopTime: any, 
    TotalExeTime: any, 
    Nsequence: any, 
    Ngroup: any
    OBstatus: any, 
    message: string
}

export default class OblistNew extends Component<Props, State>{
    constructor(props: Props){
        super(props); 

        this.handleChangeID = this.handleChangeID.bind(this);
        this.handleChangeOBnumber = this.handleChangeOBnumber.bind(this);
        this.handleChangeOBname = this.handleChangeOBname.bind(this); 
        this.handleChangeProgram = this.handleChangeProgram.bind(this); 
        this.handleChangeObserver = this.handleChangeObserver.bind(this); 
        this.handleChangeOBpriority = this.handleChangeOBpriority.bind(this); 
        this.handleChangeOBactivation = this.handleChangeOBactivation.bind(this); 
        this.handleChangeOBstartTime = this.handleChangeOBstartTime.bind(this); 
        this.handleChangeOBstopTime = this.handleChangeOBstopTime.bind(this); 
        this.handleChangeTotalExeTime = this.handleChangeTotalExeTime.bind(this); 
        this.handleChangeNsequence = this.handleChangeNsequence.bind(this); 
        this.handleChangeNgroup = this.handleChangeNgroup.bind(this); 
        this.handleChangeOBstatus = this.handleChangeOBstatus.bind(this); 
        this.handleChangeins_date = this.handleChangeins_date.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this); 

        this.state = {
            ins_date:  Moment(Date.now()).format('YYYY-MM-DDTHH:mm'),
            ID: this.props.oblist.ID,
            OBnumber: this.props.oblist.OBnumber, 
            OBname: this.props.oblist.OBname,
            Program: this.props.oblist.Program,
            Observer: this.props.oblist.Observer,
            OBpriority: this.props.oblist.OBpriority,
            OBactivation: this.props.oblist.OBactivation,
            OBstartTime: this.props.oblist.OBstartTime.replace(":00.000Z", ""),
            OBstopTime: this.props.oblist.OBstopTime.replace(":00.000Z", ""), 
            TotalExeTime: this.props.oblist.TotalExeTime,
            Nsequence: this.props.oblist.Nsequence,
            Ngroup: Number.isInteger(this.props.oblist.Ngroup) ? this.props.oblist.Ngroup : 0,
            OBstatus: this.props.oblist.OBstatus, 
            message: ""
        }  

    }

    handleChangeID(event: any) {
        console.log(this.state.ID); 
        console.log(event.target.value)
        this.setState({ID: event.target.value});
      }

    handleChangeOBnumber(event: any) {
        this.setState({OBnumber: event.target.value});
      }
    
    handleChangeOBname(event: any) {
        this.setState({OBname: event.target.value});
      }
    
    handleChangeProgram(event: any) {
        this.setState({Program: event.target.value});
      }
    
    handleChangeObserver(event: any) {
        this.setState({Observer: event.target.value});
      }

    handleChangeOBpriority(event: any) {
        this.setState({OBpriority: event.target.value});
      }  
    
    handleChangeOBactivation(event: any) {
        this.setState({OBactivation: event.target.value});
      } 

    handleChangeOBstartTime(event: any) {
        this.setState({OBstartTime: event.target.value});
      }
      
    handleChangeOBstopTime(event: any) {
        this.setState({OBstopTime: event.target.value});
      } 

    handleChangeTotalExeTime(event: any) {
        this.setState({TotalExeTime: event.target.value});
      } 

    handleChangeNsequence(event: any) {
        this.setState({Nsequence: event.target.value});
      } 

    handleChangeNgroup(event: any) {
        this.setState({Ngroup: event.target.value});
      } 

    handleChangeOBstatus(event: any) {
        this.setState({OBstatus: event.target.value});
      } 

    handleChangeins_date(event: any) {
        this.setState({ins_date: event.target.value});
      } 

      handleSubmit(event: any){
        const oblist = {
            ins_date: Moment(Date.now()).format('YYYY-MM-DDTHH:mm'), 
            ID: this.state.ID,
            OBnumber: this.state.OBnumber, 
            OBname: this.state.OBname,
            Program: this.state.Program,
            Observer: this.state.Observer,
            OBpriority: this.state.OBpriority,
            OBactivation: this.state.OBactivation,
            OBstartTime: this.state.OBstartTime,
            OBstopTime: this.state.OBstopTime, 
            TotalExeTime: this.state.TotalExeTime,
            Nsequence: this.state.Nsequence,
            Ngroup: Number.isInteger(this.state.Ngroup) !== undefined ? this.state.Ngroup : 0,
            OBstatus: this.state.OBstatus, 
        }

        

        oblistDataService.create(oblist).then(res => {
            if(res.data.status === 200)
            {
                this.setState( {message: res.data.message} )
            }
            this.props.close();
        }); 
        
      }  
     
    render(){

        const oblist = this.props.oblist

        return(
            <form className="form-group" onSubmit={this.handleSubmit}>
                    <label htmlFor="title">OBname</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.OBname}
                        onChange={ this.handleChangeOBname}
                        name="title"
                    />
                    <label htmlFor="title">Program</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.Program}
                        onChange={ this.handleChangeProgram}
                        name="title"
                    />
                    <label htmlFor="title">Observer</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.Observer}
                        onChange={ this.handleChangeObserver}
                        name="title"
                    />
                    <label htmlFor="title">OBpriority</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.OBpriority}
                        onChange={ this.handleChangeOBpriority}
                        name="title"
                    />
                    <label htmlFor="title">OBactivation</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.OBactivation}
                        onChange={ this.handleChangeOBactivation}
                        name="title"
                    />
                    <label htmlFor="title">OBstartTime</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.OBstartTime}
                        onChange={ this.handleChangeOBstartTime}
                        name="title"
                    />
                    <label htmlFor="title">OBstopTime</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.OBstopTime}
                        onChange={ this.handleChangeOBstopTime}
                        name="title"
                    />
                    <label htmlFor="title">TotalExeTime</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.TotalExeTime}
                        onChange={ this.handleChangeTotalExeTime}
                        name="title"
                    />
                    <label htmlFor="title">Nsequence</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.Nsequence}
                        onChange={ this.handleChangeNsequence}
                        name="title"
                    />
                    <label htmlFor="title">Ngroup</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        value={this.state.Ngroup}
                        onChange={ this.handleChangeNgroup}
                        name="title"
                    />
                    <label htmlFor="title">OBstatus</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.OBstatus}
                        onChange={ this.handleChangeOBstatus}
                        name="title"
                    />
                    <label htmlFor="title">ins_date</label>
                    <input
                        type="text"
                        className="form-control"
                        id="title"
                        required
                        value={this.state.ins_date}
                        onChange={ this.handleChangeins_date}
                        name="title"
                    />
                    <p onClick = { this.handleSubmit} className="btn btn-success">
                        Submit
                    </p>
                    <button onClick={ this.props.close } className="btn btn-secondary">
                        Cancel
                    </button>
            </form>
        )
    }
}