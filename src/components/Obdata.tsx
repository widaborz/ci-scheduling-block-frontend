import { Component } from "react";
import ObdataDataService from "../services/obdata.service";
import Obdatanew from "./ObdataNew"

type Props = {};

type State = {
  obdata: any,
  currentObdata: any | null
};

export default class ObData extends Component <Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {obdata: [], currentObdata: null}; 
        ObdataDataService.getAll().then( (res) => {
            this.setState( { obdata: res.data.data } ) 
        });
    }

        

    render(){

        
        return(
            <div>
                <Obdatanew obdata = {this.state["obdata"]} />
                
                <div className="col-md-6">
                <h4>Tutorials List</h4>

                <ul className="list-group">
                    {1 &&
                    this.state.obdata.map((obdata: any, index: number) => (
                        <li
                        className={
                            "list-group-item " +
                            (index === 1 ? "active" : "")
                        }
                        onClick={() => {}}
                        key={index}
                        >
                        {obdata.Object}
                        </li>
                    ))}
                </ul>

                <button
                    className="m-3 btn btn-sm btn-danger"
                    onClick={() => {}}
                >
                    Remove All
                </button>
                </div>
                <div className="col-md-6">
                {1 ? (
                    <div>
                    <h4>Tutorial</h4>
                    <div>
                        <label>
                        <strong>Title:</strong>
                        </label>{" "}
                        "currentTutorial.title"
                    </div>
                    <div>
                        <label>
                        <strong>Description:</strong>
                        </label>{" "}
                        "currentTutorial.description"
                    </div>
                    <div>
                        <label>
                        <strong>Status:</strong>
                        </label>{" "}
                        {1 ? "Published" : "Pending"}
                    </div>

                    
                    </div>
                ) : (
                    <div>
                    <br />
                    <p>Please click on a Tutorial...</p>
                    </div>
                )}
                </div>
            </div>
        )
    }
}