import { Component } from "react";
import OblistDataService from "../services/oblist.service"
import OblistDetail from "./OblistDetail"
import OblistNew from "./OblistNew"
import OBDataList from "./OBDataList"


type Props = {};

type State = {
  oblist: any,
  currentOblist: any | null, 
  addNew: boolean
};

export default class ObList extends Component <Props, State>  {

    constructor(props: Props){
        super(props); 

        this.state = {
            oblist: [], 
            currentOblist: null, 
            addNew: false
        }

        this.selectOblist = this.selectOblist.bind(this);

        OblistDataService.getAll().then( (res) => { 
            this.setState({ oblist: res.data.data }); 
         } )
    }

    selectOblist(id: number) {
        this.setState({ currentOblist: id }); 
    }

    handleOnClose(){
        console.log("handle on close"); 
        OblistDataService.getAll().then( (res) => { 
            this.setState({ oblist: res.data.data }); 
         } )
    }


    render(){
        return(
            <div>
                {!this.state.addNew &&
                    <div>
                        <h5>Add new oblist</h5>
                        <button className="btn btn-success" onClick={ () => {this.setState( {addNew: !this.state.addNew } )} }>
                        Add
                        </button>
                    </div>
                }
                {this.state.addNew && <div>
                    <OblistNew oblist = {this.state.oblist[0]} close = {() => {this.setState( {addNew: !this.state.addNew } )}}/>
                </div>}
                <div style = { {margin: "20px"}}>
                <h4>OB List</h4>
                <ul className="list-group">
                    {1 &&
                    this.state.oblist.map((obdata: any, index: number) => (
                        <li
                        className={
                            "list-group-item "
                        }
                        key={index}
                        >
                        {obdata.ID !== this.state.currentOblist &&
                            <div onClick={() => this.selectOblist(obdata.ID) }>{obdata.ID} {obdata.OBname} {obdata.Program} {obdata.Observer}</div>
                        }
                        {obdata.ID === this.state.currentOblist && 
                            this.state.oblist.filter( (ob: any) => ob.ID === this.state.currentOblist).map((oblist: any) => (
                                <div key = {oblist.id}>
                                    <OblistDetail oblist = {oblist} />
                                    <button className = "btn btn-secondary" onClick = {() => {this.setState({currentOblist: null})}}>Close</button>
                                </div>
                            ))
                        }
                        </li>
                        
                    ))}
                </ul>
                </div>
            </div>
        )
    }
}