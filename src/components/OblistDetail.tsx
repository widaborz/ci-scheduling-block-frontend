import { Component } from "react";
import OBDataList from "./OBDataList"

type Props = {
    oblist: any
};

export default class OblistDetail extends Component <Props> {

    constructor(props: Props){
        super(props); 
    }

    render(){
        const {oblist} = this.props; 
        return (
            <div>
                <h4>OB {oblist.ID}</h4>
                <div>
                    <label>
                    <strong>ins_date:</strong>
                    </label>{" "}
                    {oblist.ins_date}
                </div>
                <div>
                    <label>
                    <strong>OBnumber:</strong>
                    </label>{" "}
                    {oblist.OBnumber}
                </div>
                <div>
                    <label>
                    <strong>OBname:</strong>
                    </label>{" "}
                    {oblist.OBname}
                </div>
                <div>
                    <label>
                    <strong>Program:</strong>
                    </label>{" "}
                    {oblist.Program}
                </div>
                <div>
                    <label>
                    <strong>Observer:</strong>
                    </label>{" "}
                    {oblist.Observer}
                </div>
                <div>
                    <label>
                    <strong>OBpriority:</strong>
                    </label>{" "}
                    {oblist.OBpriority}
                </div>
                <div>
                    <label>
                    <strong>OBstatus:</strong>
                    </label>{" "}
                    {oblist.OBstatus}
                </div>
                <div>
                    <label>
                    <strong>OBactivation:</strong>
                    </label>{" "}
                    {oblist.OBactivation}
                </div>
                <div>
                    <label>
                    <strong>OBstartTime:</strong>
                    </label>{" "}
                    {oblist.OBstartTime}
                </div>
                <div>
                    <label>
                    <strong>OBstopTime:</strong>
                    </label>{" "}
                    {oblist.OBstopTime}
                </div>
                <div>
                    <label>
                    <strong>TotalExeTime:</strong>
                    </label>{" "}
                    {oblist.TotalExeTime}
                </div>
                <div>
                    <label>
                    <strong>Nsequence:</strong>
                    </label>{" "}
                    {oblist.Nsequence}
                </div>
                <div>
                    <label>
                    <strong>Ngroup:</strong>
                    </label>{" "}
                    {oblist.Ngroup}
                </div>
                <OBDataList oblist = {oblist} />
            </div>
        )
    }
}