import { Component } from "react";
import ObdataDataService from "../services/obdata.service";

type Props = {
    oblist: any
};

type State = {
  obdata: any,
};

export default class ObDataList extends Component <Props, State> {

    constructor(props: Props){
        super(props); 

        this.state = { 
            obdata: []
        }; 

        ObdataDataService.getByOBnumber(props.oblist.OBnumber).then( (res) => { 
            this.setState({ obdata: res.data.data }); 
         } )
    }

    render(){
        return(
                <div>
                <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Object</th>
                        <th scope="col">_RA</th>
                        <th scope="col">_DEC</th>
                    </tr>
                </thead>
                <tbody>
                    {1 &&
                        this.state.obdata.map((obdata: any, index: number) => (
                            <tr
                            key={index}
                            >
                                <th scope="row">{obdata.ID}</th>
                                <td>{obdata.Object}</td>
                                <td>{obdata._RA}</td>
                                <td>{obdata._DEC}</td>
                            </tr>
                        ))}
                </tbody>
                </table>
            </div>
        )
    }
}