import { Component } from 'react'
import { Formik, Field, Form, FormikHelpers } from 'formik';
import obdataService from '../services/obdata.service';

interface Values {
  OBnumber: number;
  Note: string;
  _RA: number;
  _DEC: number;
  Epoch: number;
  Exp_type: number;
  Exp_time: number; 
  Repeats: number;
  Binning: number;
  Guider: number;
  Filter: number;
  Object: string;
  Dithering: number;
  Focus: number;
  Priority: number;
  Alt_limit: number;
  Status: number;
  Activation: number;
  Xstart: number;
  Ystart: number;
  Xstop: number;
  Ystop: number;
  StartTime: string;
  StopTime: string;
  Done: number;
  Sequence: number;
  CCD_overhead: number;
}

type Props = {
  obdata: any
};

type State = {
  obdata: any
};


export default class ObdataNew extends Component<Props, State>{

  constructor(props: Props){
    super(props); 

    this.state = {obdata: []}; 

  }
    
    render() {
      return (
        <Formik
          initialValues={{
            OBnumber: 27,
            Note: "",
            _RA: 185.4603,
            _DEC: 4.4817,
            Epoch: 2000.000,
            Exp_type: 0,
            Exp_time: 60.0,
            Repeats: 3,
            Binning: 1,
            Guider: 1,
            Filter: 4,
            Object: "2020jfo",
            Dithering: 0.0,
            Focus: 1,
            Priority: 1,
            Alt_limit: 20,
            Status: 3,
            Activation: 0,
            Xstart: 1750,
            Ystart: 1750,
            Xstop: 2250,
            Ystop: 2250,
            StartTime: "2020-09-03 10:45:42",
            StopTime: "2020-09-03 21:44:55",
            Done: 4,
            Sequence: 1,
            CCD_overhead: 3.7,
          }}
          onSubmit={(
            values: Values,
            { setSubmitting }: FormikHelpers<Values>
          ) => {
            obdataService.create(values); 
          }}
        >
          <Form className="form-group" >
            <label htmlFor="OBnumber">OBnumber</label>
            <Field id="OBnumber" name="OBnumber" placeholder="OBnumber" className="form-control"/>
  
            <label htmlFor="Note">Note</label>
            <Field id="Note" name="Note" placeholder="Note" className="form-control"/>

            <label htmlFor="_RA">_RA</label>
            <Field id="_RA" name="_RA" placeholder="_RA" className="form-control"/>

            <label htmlFor="_DEC">_DEC</label>
            <Field id="_DEC" name="_DEC" placeholder="_DEC" className="form-control"/>

            <label htmlFor="Epoch">OBnumber</label>
            <Field id="Epoch" name="Epoch" placeholder="Epoch" className="form-control"/>

            <label htmlFor="Exp_type">Exp_type</label>
            <Field id="Exp_type" name="Exp_type" placeholder="Exp_type" className="form-control"/>

            <label htmlFor="Exp_time">Exp_time</label>
            <Field id="Exp_time" name="Exp_time" placeholder="Exp_time" className="form-control"/>

            <label htmlFor="Repeats">Repeats</label>
            <Field id="Repeats" name="Repeats" placeholder="Repeats" className="form-control"/>

            <label htmlFor="Binning">Binning</label>
            <Field id="Binning" name="Binning" placeholder="Binning" className="form-control"/>

            <label htmlFor="Guider">Guider</label>
            <Field id="Guider" name="Guider" placeholder="Guider" className="form-control"/>

            <label htmlFor="Filter">Filter</label>
            <Field id="Filter" name="Filter" placeholder="Filter" className="form-control"/>

            <label htmlFor="Object">Object</label>
            <Field id="Object" name="Object" placeholder="Object" className="form-control"/>

            <label htmlFor="Dithering">Dithering</label>
            <Field id="Dithering" name="Dithering" placeholder="Dithering" className="form-control"/>

            <label htmlFor="Focus">Focus</label>
            <Field id="Focus" name="Focus" placeholder="Focus" className="form-control"/>

            <label htmlFor="Priority">Priority</label>
            <Field id="Priority" name="Priority" placeholder="Priority" className="form-control"/>

            <label htmlFor="Alt_limit">Alt_limit</label>
            <Field id="Alt_limit" name="Alt_limit" placeholder="Alt_limit" className="form-control"/>

            <label htmlFor="Status">Status</label>
            <Field id="Status" name="Status" placeholder="Status" className="form-control"/>

            <label htmlFor="Activation">Activation</label>
            <Field id="Activation" name="Activation" placeholder="Activation" className="form-control"/>

            <label htmlFor="Xstart">Xstart</label>
            <Field id="Xstart" name="Xstart" placeholder="Xstart" className="form-control"/>

            <label htmlFor="Ystart">Ystart</label>
            <Field id="Ystart" name="Ystart" placeholder="Ystart" className="form-control"/>

            <label htmlFor="Xstop">Xstop</label>
            <Field id="Xstop" name="Xstop" placeholder="Xstop" className="form-control"/>

            <label htmlFor="Ystop">Ystop</label>
            <Field id="Ystop" name="Ystop" placeholder="Ystop" className="form-control"/>

            <label htmlFor="StartTime">StartTime</label>
            <Field id="StartTime" name="StartTime" placeholder="StartTime" className="form-control"/>

            <label htmlFor="StopTime">StopTime</label>
            <Field id="StopTime" name="StopTime" placeholder="StopTime" className="form-control"/>

            <label htmlFor="Done">Done</label>
            <Field id="Done" name="Done" placeholder="Done" className="form-control"/>

            <label htmlFor="Sequence">Sequence</label>
            <Field id="Sequence" name="Sequence" placeholder="Sequence" className="form-control"/>

            <label htmlFor="CCD_overhead">CCD_overhead</label>
            <Field id="CCD_overhead" name="CCD_overhead" placeholder="CCD_overhead" className="form-control"/>
  
            <button type="submit" className="btn btn-success">Submit</button>
          </Form>
        </Formik>
      );
    }
    
  };

