import http from "../http-common"

class ObdataDataService {


    getAll() {
      return http.get("obdata"); 
    }

    getByOBnumber(OBnumber){
      return http.get("obdata/OBnumber/" + OBnumber); 
  }

  create(obdata){
    return http.post("obdata", obdata); 
}
}

export default new ObdataDataService();