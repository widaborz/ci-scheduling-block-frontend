import http from "../http-common"

class OblistDataService{

    getAll(){
        return http.get("oblist"); 
    }

    getById(id){
        return http.get("oblist/", id); 
    }

    create(oblist){
        return http.post("oblist", oblist); 
    }
}

export default new OblistDataService()